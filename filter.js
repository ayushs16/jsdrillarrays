const { elements } = require( './app' );
function callback( value , index){
    if( value % 2 == 0 ){
        return true;
    }
    return false;
}

function filter( elements , cb ){
    let filteredArray = [];

    for( let index = 0 ; index < elements.length ; index++ ){
        if( cb( elements[ index ] ) ){
            filteredArray.push( elements[ index ] );
        }
    }
    return filteredArray;
}

const filterArr = filter( elements , callback );
console.log( filterArr );