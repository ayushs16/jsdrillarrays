const { elements } = require( './app' );

function callback( element , index ){
    return (element * index );
}

function map( elements , cb ){
    let mappedArray = [];
    for( index = 0 ; index < elements.length ; index++ ){
        mappedArray.push(cb(elements[index],index));
    }
    return mappedArray;
}

const mapArr = map( elements , callback );
console.log(mapArr);