const { elements } = require( './app' );

const callback = function( currentValue , element ){
    return currentValue + element;
}

function reduce( elements , cb , startingValue ){
    if( startingValue === undefined ){
        startingValue = elements[0];
    }
    for( let index = 0 ; index < elements.length ; index++ ){
        startingValue = cb(startingValue , elements[ index ])
    }
    return startingValue;
}

let totalSum = reduce( elements , callback );
console.log(totalSum);