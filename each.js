const { elements } = require( './app' );

function output( value , index){
    console.log( `${ value }  ${ index }` );
}

function each( elements , cb ){
    for( let index = 0 ; index < elements.length ; index++ ){
        cb( elements[ index ] , index );
    }
}

each( elements , output );