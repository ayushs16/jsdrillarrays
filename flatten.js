const nestedArray = [1, [2], [[3]], [[[4]]]];

function callback( element ){
    if( typeof( element ) === typeof( 1 ) ){
        return element;
    }
    return callback( element[0] );
}

function flatten( elements , cb ){
    let flattenedArray = [];

    for( let index = 0 ; index < elements.length ; index++ ){
        flattenedArray.push( cb( elements[index] ) );
    }
    return flattenedArray;
}

const resultedArray = flatten( nestedArray , callback );
console.log(resultedArray);