const { elements } = require( './app' );

function callback( element ){
    if(element == 5){
        return true;
    }
    return false;
}

function find( elements , cb ){
    for( let index = 0 ; index < elements.length ; index++ ){
        if( cb( elements[ index ] ) ){
            return elements[index];
        };
    }
    return `undefined`;
}

const ans = find( elements , callback );
console.log( ans );